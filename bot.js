// PRM Bot, Version 0.0.1 2021-01-09

// Add bot to discord server:
// https://discord.com/oauth2/authorize?client_id=795347075000434718&scope=bot&permissions=8
// Tuto: https://www.digitaltrends.com/gaming/how-to-make-a-discord-bot/
// Tuto: https://medium.com/davao-js/tutorial-creating-a-simple-discord-bot-9465a2764dc0
// node index.js

//Send tx without having to unlock the account 
//https://stackoverflow.com/questions/46611117/how-to-authenticate-and-send-contract-method-using-web3-js-1-0

// Get ERC20 Token Balance
// https://medium.com/@piyopiyo/how-to-get-erc20-token-balance-with-web3-js-206df52f2561
// https://piyolab.github.io/playground/ethereum/getERC20TokenBalance/

// Send Send ERC20 Token
// https://piyolab.github.io/playground/ethereum/sendERC20Token/

const Web3          = require('web3');
const Discord       = require('discord.js'); //https://github.com/discordjs/discord.js
//const BigNumber     = require('bignumber.js');
const fs            = require("fs");
const botSettings   = require('./botSettings.json');
const { resourceUsage } = require('process');

const minABI = [
    // balanceOf
    {
        "constant":true,
        "inputs":[{"name":"_owner","type":"address"}],
        "name":"balanceOf",
        "outputs":[{"name":"balance","type":"uint256"}],
        "type":"function"
    },
    // decimals
    {
        "constant":true,
        "inputs":[],
        "name":"decimals",
        "outputs":[{"name":"","type":"uint8"}],
        "type":"function"
}];

const minABI2 = [
    // transfer
    {
      "constant": false,
      "inputs": [
        {
          "name": "_to",
          "type": "address"
        },
        {
          "name": "_value",
          "type": "uint256"
        }
      ],
      "name": "transfer",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "type": "function"
    }
];

// Initialize web3
const web3 = new Web3(Web3.givenProvider || botSettings.web3provider);

// Initialize Discord Bot
const bot = new Discord.Client({disableEveryone:true});

bot.on('ready', () => {
    console.log(`PRM Bot inicializado, ${bot.user.tag}`);
});

//bot.on('message', function (user, userID, channelID, message, evt) {
bot.on('message',async message => {

    //const user = message.author.name; //legacy
    const serverId = message.guild.id;
    const userName = message.author.username;
    const userID = message.author.id;
    const channelID = message.channel.id;
    
    if(message.channel.type === "dm") return;

    if( !isChannelAccess(channelID) ){ 
		//return message.channel.send("Por favor escribe los comandos en el canal -bots, gracias!");
		return;
    }
    
    //Get arguments
	const args = message.content.split(' ');

    //Get current prefix
    const prefix = message.content.split('.'); //console.log("Prefix Actual: "+prefix[0]);
    const botPrefix = prefix[0].toLowerCase()+"."; //asignar prefijo dinamico
    let tokenKeyName = prefix[0].toUpperCase();
    //console.log("Token Name:", tokenKeyName);

    if( botSettings.tokens[prefix[0].toUpperCase()] === undefined){ //Validate if token name/prefix exist
        return;
    }

    /*
    for (var key in botSettings.tokens) {
        if(key == tokenName){
            console.log(key);
            console.log(botSettings.tokens[key]);
        }
    }*/

//-- Debug
    if(message.content.toLowerCase().startsWith(botPrefix+"debug")){
        //console.log("Tip:" + userID);
        //console.log("Debug: ", args[1]);
        //console.log("Debug: ", args[1]); //print: <@!300412335275769856>
        //console.log("tag",message.author.tag);
        //let user_id = args[1].replace(/\D/g, ""); //extraer el id del usuario
        //return message.channel.send( user_id );
        //return message.channel.send( `<@${message.author.id}>` );

        //console.log("Debug: ", botSettings.tokens[prefix[0].toUpperCase()] );

        //console.log("MSG ID: ", message.guild.id);
        
        /*      
        const guild = bot.guilds.cache.get(serverId);
        if (!guild) return console.log("Couldn't get the guild.");

        const members = guild.members.cache.filter(member => member.roles.cache.find(role => role.name === "arole")).map(member => member.id);
        //const members = guild.members.cache.filter(member => member.roles.cache.find(role => role.name === "arole")).map(member => member.id);
        //const members = guild.members.cache.map(member => member.id);

        //console.log("DEBUG: ",bot.guilds.cache);
        console.log("DEBUG: ",members);
        */

        const guild = bot.guilds.cache.get(serverId);
        if (!guild) return console.log("Couldn't get the guild.");
        //const members = guild.members.cache.filter(member => member.roles.cache.find(role => role.name === "arole")).map(member => member.id);
        //const members = guild.members.cache.filter(member => member.roles.cache.find(role => role.name !== "")).map(member => member.id);
        const members = guild.members.cache.filter(member => member.presence.status !== "online").map(member => member.id);

        const data = getJson(botSettings.usersById); // registered users
        let result = [];

        //data.forEach( aUsers =>{
        //    console.log(aUsers);
        //});

        for (let key in data){
            if(data.hasOwnProperty(key)){
            //console.log(`${key} : ${data[key]}`)
            result.push(key);
            }
        }

        console.log("Channel members", members);
        // console.log("All members: ",result);
        // console.log("cahe: ",guild.members.cache)

        //const onlineMembers = guild.members.cache.filter(member => member.presence.status === "online");
        //console.log("ONLINE USERS:",onlineMembers);

        

        // const listedChannels = []; 
        // guild.channels.cache.forEach(channel => { 
        //     listedChannels.push(channel.name);

        //     if(channel.id == 538132696489000980){
        //         //let members2 = channel.members.filter(member => member.presence.status !== "online").map(member => member.id);
        //         //console.log("Members2: ", members2);
        //         console.log("CHN MEMS: ", channel.members['795347075000434718']);
        //     }
        // });

        // console.log("CANALES: ",listedChannels);


        var users = bot.users;
        console.log("USERS ",users);
        // users.keyArray().forEach((val) => {
        //     var userName = users.get(val).id;
        //     var status = users.get(val).presence.status;
        //     if(status == "online"){
        //         onlineList.push(userName);
        //     }
        // });
        // console.log("ONLINE :", onlineList);
        
    }


//--------------------------
// COMANDOS PARA EL USUARIO
//--------------------------


//-- Register user by discord ID
    if(message.content.toLowerCase().startsWith(botPrefix+"register ")){
        const address = args[1];
        let msg = "";

        if(web3.utils.isAddress(args[1])){	
            let data = getJson(botSettings.usersById);
            //console.log("add: "+ Object.values(data).includes(address));
            //console.log("userID: "+ Object.keys(data).includes(userID));
            if(!Object.values(data).includes(address) && !Object.keys(data).includes(userID)){		
                data[userID] = address;
                msg = _lang(`:tada: <@!${userID}> you have correctly registered the EXP address: ${address}`, `:tada: <@!${userID}> has registrado correctamente tu dirección EXP: ${address}`, serverId);
                
                fs.writeFile(botSettings.usersById, JSON.stringify(data), (err) => {
                    if (err) throw err;
                        console.log( `Un nuevo usuario ha registrado su wallet: ${userName}` ); //The file has been saved.
                    });	
                
            } else {
                msg = _lang(`:thumbsup: <@!${userID}> your wallet is already registered, if you want please update your wallet with command **${botPrefix}changeadd**`, `:thumbsup: <@!${userID}> ya habias registrado tu wallet, si deseas actualizarla usa el comando **${botPrefix}changeadd**`, serverId);
            }
        } else {
            msg = _lang(`:open_mouth: <@!${userID}> you are trying to register an invalid EXP address. Please try again and make sure it has the format: **${botPrefix}register 0xAddress...**`, `:open_mouth: <@!${userID}> intentas registrar una dirección de EXP no válida. Por favor intenta de nuevo y asegurate que tenga el siguiente formato: **${botPrefix}register 0xAddress...**`, serverId);
        }
        
        return message.channel.send(msg);            
    }


//-- Change registration with bot.
    if(message.content.toLowerCase().startsWith(botPrefix+"changeadd ")){
        const address = args[1];
        let msg = "";
        
        if(web3.utils.isAddress(args[1])){
            let data = getJson(botSettings.usersById);
            if(Object.keys(data).includes(userID)){
                if(address != data[userID]){
                    data[userID] = address;
                    fs.writeFile(botSettings.usersById, JSON.stringify(data), (err) => {
                        if (err) throw err;
                            console.log(`El usuario ${userName} se ha cambiado de wallet.`); //The file has been changed
                        });
                    msg = `:thumbsup: <@!${userID}> has cambiado tu dirección anterior de EXP por: **${address}**`;
                } else {
                    msg = `<@!${userID}> parece que ya tienes registrada esta dirección, usa una diferente si estas tratando de cambiar la anterior.`;
                }
            } else {
                msg = `:open_mouth: no estas en la lista, registra primero tu dirección de EXP con el comando **${botPrefix}register** *<address>*`; 
            }
        } else {
            msg = `:open_mouth: <@!${userID}> intentas registrar una dirección de EXP incorrecta, intenta de nuevo con el formato: **${botPrefix}register 0xAddress**`;
        }

        return message.channel.send(msg);
    }

//-- Check to see user wallet.
    if(message.content.toLowerCase() == botPrefix+"myaddress"){
        const data = getJson(botSettings.usersById);
        const wallet = data[userID];
        let msg = "";

        if(Object.keys(data).includes(userID)){
            msg = `:pouch: <@!${userID}> tu wallet de token para EXP es: **${wallet}**`; //already registered
        } else {
            msg = `:open_mouth: <@!${userID} no estas registrado, usa el comando **${botPrefix}register** *<address>*`; //Plese register
        }

        return message.channel.send(msg);
    }

//-- Get token balance
    if(message.content.toLowerCase() === botPrefix+"bal" || 
       message.content.toLowerCase() === botPrefix+"balance"){

        //Get current token info
        const tokenInfo = botSettings.tokens[tokenKeyName];
        //console.log("Token Info:", tokenInfo);
        //console.log("Obj: ",tokenInfo);

        const data = getJson(botSettings.usersById);
        const walletAddress = data[userID];

        if(tokenInfo === undefined){
            return message.channel.send(_lang("Sorry, the coin or token is not available in our bot.", "Lo sentimos, el token o la moneda aun no está dispobinle en nuestro bot.", serverId));
        }

        if(walletAddress === undefined){
            return message.channel.send(_lang("Seems that you haven't registered your wallet.", "Parece que aún no ha registrado tu wallet.", serverId));
        }

        //Get Token balance            
        //Get ERC20 Token contract instance
        //const contract = new web3.eth.Contract(minABI, tokenAddress, {from:walletAddress});
        const MyContract = new web3.eth.Contract(minABI, tokenInfo.address);

        // Call balanceOf function
        MyContract.methods.balanceOf(walletAddress).call()
        .then(function(balance){
            //console.log( "Total PRM:", (balance/Math.pow(10,18)).toFixed(3) );
            
            balance = (balance/Math.pow(10,tokenInfo.decimals)).toFixed(8);
            
            message.channel.send( 
                _lang(`:moneybag: <@!${userID}> you have **${balance} ${botPrefix.toUpperCase()}**, check all your balances in **www.expexplorer.guarda.co**`,
                      `:moneybag: <@!${userID}> tienes **${balance} ${botPrefix.toUpperCase()}**, puedes ver tus balances completos en **www.expexplorer.guarda.co**`,
                      serverId)
            );    
        });            

    }


//-- Send Tip a user.
    if(message.content.toLowerCase().startsWith(botPrefix+"tip ")){
        
        if( !isTokenOwner(userID) ){ 
            return message.channel.send(_lang(":smile: Sorry, only administrators can use the *tip* command.", ":smile: Lo siento, solo administradores pueden usar el comando *tip*.", serverId));
        }

        let msg = "";
        let user_id = args[1].replace(/\D/g, ""); //Extraer el Id del usuario
        let amount = Number(args[2]);

        //Get current token info
        let tokenInfo = botSettings.tokens[tokenKeyName];
        //console.log("Token Info:", tokenInfo);
        
        if(tokenInfo === undefined){
            return message.channel.send(
                _lang("Sorry, the coin or token doesn't exist in our bot.", "Lo sentimos el token o la moneda no existe en nuestro bot.", serverId)
            );
        }

        if (!amount){ // if use wrong amount (string or something)
            return message.channel.send(
                _lang(`:thinking: You have entered an invalid amount of ${tokenKeyName}, please try again.`, `:thinking: Has ingresado una cantidad inválida de ${tokenKeyName}, intenta de nuevo.`, serverId)
            );
        }

        //Permitir enviar tips del token a Just a Seal
        if( userID=="804032424215969822" && tokenKeyName.toUpperCase() != "X420" ){
            return message.channel.send( _lang("Sorry, you are not authorized to send tips for this token.", "Lo siento no tienes autorizado enviar tips para este token.", serverId) );
        }

        //Permitir enviar tips del token a Stefal
        if( userID=="778048794625179690"){
            if(tokenKeyName.toUpperCase() == "EABN" || tokenKeyName.toUpperCase() == "EVLTZ"){
                //let's go
            }else{
                return message.channel.send( _lang("Sorry, you are not authorized to send tips for this token.", "Lo siento no tienes autorizado enviar tips para este token.", serverId) );
            }
        }

        //Permitir enviar tips del token a Wedergarten
        if( userID=="537600835966861314"){
            if(tokenKeyName.toUpperCase() == "EXC" || tokenKeyName.toUpperCase() == "ALYM"){
                //let's go
            }else{
                return message.channel.send( _lang("Sorry, you are not authorized to send tips for this token.", "Lo siento no tienes autorizado enviar tips para este token.", serverId) );
            }
        }
        
        //Permitir enviar tips del token a Omar
        if( userID=="351489283166699520" && tokenKeyName.toUpperCase() != "STK" ){
            return message.channel.send( _lang("Sorry, you are not authorized to send tips for this token.", "Lo siento no tienes autorizado enviar tips para este token.", serverId) );
        }

        //Permitir enviar tips del token a Mello
        if( userID=="419467013728108544" && tokenKeyName.toUpperCase() != "HY511" ){
            return message.channel.send( _lang("Sorry, you are not authorized to send tips for this token.", "Lo siento no tienes autorizado enviar tips para este token.", serverId) );
        }

        //Permitir enviar tips del token a JJJH82
        if( userID=="547477878804054028" && tokenKeyName.toUpperCase() != "SVIT" ){
            return message.channel.send( _lang("Sorry, you are not authorized to send tips for this token.", "Lo siento no tienes autorizado enviar tips para este token.", serverId) );
        }

        //Permitir enviar tips del token a madwillysg
        if( userID=="360276639147360266" && tokenKeyName.toUpperCase() != "NST" ){
            return message.channel.send( _lang("Sorry, you are not authorized to send tips for this token.", "Lo siento no tienes autorizado enviar tips para este token.", serverId) );
        }

        //Permitir enviar tips del token a Jordan16452
        if( userID=="627983177054420993" && tokenKeyName.toUpperCase() != "RARE" ){
            return message.channel.send( _lang("Sorry, you are not authorized to send tips for this token.", "Lo siento no tienes autorizado enviar tips para este token.", serverId) );
        }

        //Permitir enviar tips del token a Tego
        if( userID=="410761149349363731" && tokenKeyName.toUpperCase() != "SUGAR" ){
            return message.channel.send( _lang("Sorry, you are not authorized to send tips for this token.", "Lo siento no tienes autorizado enviar tips para este token.", serverId) );
        }

        //Permitir enviar tips del token a Blockchain Honduras
        if( userID=="734675415276322837" && tokenKeyName.toUpperCase() != "XTC" ){
            return message.channel.send( _lang("Sorry, you are not authorized to send tips for this token.", "Lo siento no tienes autorizado enviar tips para este token.", serverId) );
        }

        let data = getJson(botSettings.usersById);
        
        if(Object.keys(data).includes(user_id)){
            let toAddress = data[user_id];
            
            //--- SEND TOKEN -----------------------
            //sendToken(tokenInfo.address, userAddress, amount); //token address, user address

            // Use BigNumber
            let weiAmount = amount*Math.pow(10,tokenInfo.decimals);

            let total = web3.utils.toBN(weiAmount).toString(); //https://web3js.readthedocs.io/en/v1.3.0/web3-utils.html?highlight=toBN#tobn
            //console.log("Amount: ", weiAmount);
            //console.log("Total: ", weiAmount);

            const MyContract = new web3.eth.Contract(minABI2, tokenInfo.address); //Get ERC20 Token contract instance
        
            //gas: web3.utils.toHex(120000), //120000
            /* PROMISE
            MyContract.methods.transfer(toAddress, total)
                .send({from: botSettings.botAddress})
                .then(function(receipt){ // receipt can also be a new contract instance, when coming from a "contract.deploy({...}).send()"
                    console.log( `Tip enviado por ${user}` , receipt.transactionHash);

                    if( receipt.status ){
                        msg = `:tada: <@!${user_id}> has recibido un tip de **${amount} ${botPrefix.toUpperCase()}**`;
                    }else{
                        msg = `:grimacing: lo siento, el tip no se ha enviado, intenta de nuevo.`;
                    }
                    message.channel.send(msg);
                });
            */

            //https://web3js.readthedocs.io/en/v1.3.0/web3-eth-contract.html#id29
            MyContract.methods.transfer(toAddress, total) 
                .send({from: botSettings.botAddress})
                .on('transactionHash', function(hash){
                    console.log( `Tip enviado de ${botPrefix.toUpperCase()} TX:` , hash);
                    msg = _lang(`:tada: <@!${user_id}> you have received a tip of **${amount} ${botPrefix.toUpperCase()}**`, `:tada: <@!${user_id}> has recibido un tip de **${amount} ${botPrefix.toUpperCase()}**`, serverId);
                    message.channel.send(msg);
                })
                /*
                .on('confirmation', function(confirmationNumber, receipt){
                    console.log("Confirmation Num:",confirmationNumber);
                    console.log("Receipt:",receipt.transactionHash);
                })
                .on('receipt', function(receipt){
                    console.log("Receipt:",receipt.transactionHash);
                })
                */
                .on('error', function(error, receipt) { // If the transaction was rejected by the network with a receipt, the second parameter will be the receipt.
                    console.log("Error enviando tip:",error);
                    console.log("Receipt:",receipt);
                });
                
        } else {
            return message.channel.send(_lang(":thinking: this user is not registered.", ":thinking: este usuario no esta registrado.", serverId));
        }
            
    }


// Rain on the online and registered users.
    if(message.content.toLowerCase().startsWith(botPrefix+"rain ")){

        if( !isTokenOwner(userID) ){ 
            return message.channel.send( _lang(`:smile: I'm sorry, only admins can make rains.`, `:smile: Lo siento, sólo los administradores pueden hacer lluvias.`, serverId) );
        }
                
        //Permitir enviar rains del token a Just a Seal
        if( userID=="804032424215969822" && tokenKeyName.toUpperCase() != "X420" ){
            return message.channel.send( _lang(`:grimacing: I'm sorry, olny the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Lo sento, sólo el admin de **${tokenKeyName.toUpperCase()}** está autorizado para hacer rains.`, serverId) );
        }

        //Permitir enviar rains del token a Stefal
        if( userID=="778048794625179690"){
            if(tokenKeyName.toUpperCase() == "EABN" || tokenKeyName.toUpperCase() == "EVLTZ"){
                //let's go
            }else{
                return message.channel.send( _lang(`:grimacing: I'm sorry, olny the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Lo sento, sólo el admin de **${tokenKeyName.toUpperCase()}** está autorizado para hacer rains.`, serverId) );
            }
        }
        
        //Permitir enviar rains del token a Wedergarten
        if( userID=="537600835966861314"){
            if(tokenKeyName.toUpperCase() == "EXC" || tokenKeyName.toUpperCase() == "ALYM"){
                //let's go
            }else{
                return message.channel.send( _lang(`:grimacing: I'm sorry, olny the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Lo sento, sólo el admin de **${tokenKeyName.toUpperCase()}** está autorizado para hacer rains.`, serverId) );
            }
        }

        //Permitir enviar rains del token a Omar
        if( userID=="351489283166699520" && tokenKeyName.toUpperCase() != "STK" ){
            return message.channel.send( _lang(`:grimacing: I'm sorry, olny the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Lo sento, sólo el admin de **${tokenKeyName.toUpperCase()}** está autorizado para hacer rains.`, serverId) );
        }

        //Permitir enviar rains del token a Mello
        if( userID=="419467013728108544" && tokenKeyName.toUpperCase() != "HY511" ){
            return message.channel.send( _lang(`:grimacing: I'm sorry, olny the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Lo sento, sólo el admin de **${tokenKeyName.toUpperCase()}** está autorizado para hacer rains.`, serverId) );
        }

        //Permitir enviar rains del token a JJJH82
        if( userID=="547477878804054028" && tokenKeyName.toUpperCase() != "SVIT" ){
            return message.channel.send( _lang(`:grimacing: I'm sorry, olny the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Lo sento, sólo el admin de **${tokenKeyName.toUpperCase()}** está autorizado para hacer rains.`, serverId) );
        }

        //Permitir enviar rains del token a madwillysg
        if( userID=="360276639147360266" && tokenKeyName.toUpperCase() != "NST" ){
            return message.channel.send( _lang(`:grimacing: I'm sorry, olny the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Lo sento, sólo el admin de **${tokenKeyName.toUpperCase()}** está autorizado para hacer rains.`, serverId) );
        }

        //Permitir enviar rains del token a Jordan16452
        if( userID=="627983177054420993" && tokenKeyName.toUpperCase() != "RARE" ){
            return message.channel.send( _lang(`:grimacing: I'm sorry, olny the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Lo sento, sólo el admin de **${tokenKeyName.toUpperCase()}** está autorizado para hacer rains.`, serverId) );
        }

        //Permitir enviar rains del token a Tego
        if( userID=="410761149349363731" && tokenKeyName.toUpperCase() != "SUGAR" ){
            return message.channel.send( _lang(`:grimacing: I'm sorry, olny the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Lo sento, sólo el admin de **${tokenKeyName.toUpperCase()}** está autorizado para hacer rains.`, serverId) );
        }

        //Permitir enviar rains del token a Blockchain Honduras
        if( userID=="734675415276322837" && tokenKeyName.toUpperCase() != "XTC" ){
            return message.channel.send( _lang(`:grimacing: I'm sorry, olny the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Lo sento, sólo el admin de **${tokenKeyName.toUpperCase()}** está autorizado para hacer rains.`, serverId) );
        }

        let amount = Number(args[1]);
        if (!amount){
            return message.channel.send(`:thinking: Has ingresado una cantidad inválida de ${botPrefix.toUpperCase()} intenta de nuevo.`);
        } 

        // main raining func
        raining(amount, message, tokenKeyName, userName, serverId);
        
    }

    // Rain on the online and registered users.
    if(message.content.toLowerCase().startsWith(botPrefix+"rain.online ")){
        
        let amount = Number(args[1]);
        if (!amount){
            return message.channel.send(_lang(`:thinking: You have entered an invalid amount of ${botPrefix.toUpperCase()} intenta de nuevo.`, `:thinking: Has ingresado una cantidad inválida de ${botPrefix.toUpperCase()} intenta de nuevo.`, serverId));
        } 

        // main raining func
        rainingOnline(amount, message, tokenKeyName, userName);
    }


//-- Send Tip a user.
    if(message.content.toLowerCase().startsWith(botPrefix+"faucet")){

        if( channelID !== '835610160788406333' &&  //prm-faucet  | 614616294897221652 PRM test
            channelID !== '925049057309777960' //svit-faucet
            ){ 
            return message.channel.send(
                _lang(`Sorry, **${tokenKeyName}.faucet** command isn't active for this channel.`, `Lo siento, el comando **${tokenKeyName}.faucet** no está activo para este canal.`, serverId)
            );
        }

        //Get current token info
        let tokenInfo = botSettings.tokens[tokenKeyName];
        //console.log("Token Info:", tokenInfo);
        
        if(tokenInfo === undefined){
            return message.channel.send(
                _lang("Sorry, the coin or token doesn't exist in our bot.", "Lo sentimos el token o la moneda no existe en nuestro bot.", serverId)
            );
        }

        //Allowing only PRM and SVIT faucet
        if( tokenKeyName.toUpperCase() != "PRM" && tokenKeyName.toUpperCase() != "SVIT" ){
            return message.channel.send( 
                _lang("Sorry, faucet is only available for the PRM token, try again after faucet time out.", "Lo siento el faucet sólo esta disponible para el token PRM (Promineros), intenta después del tiempo de espera por el faucet.", serverId) 
            );
        }

        let user_id = userID;
        let data = getJson(botSettings.usersById);
        
        if(Object.keys(data).includes(user_id)){
            //SEND FAUCET
            let toAddress = data[user_id];

            let max = 0.1; //0.01                   
            let min = 0.006; //0.005

            if(tokenKeyName=="SVIT"){
                max = 0.009;                
                min = 0.007;
            }

            let amount = Math.random() * (max - min) + min;

            // Use BigNumber
            let weiAmount = amount*Math.pow(10,tokenInfo.decimals);
            
            let total = web3.utils.toBN(weiAmount).toString(); //https://web3js.readthedocs.io/en/v1.3.0/web3-utils.html?highlight=toBN#tobn
            const MyContract = new web3.eth.Contract(minABI2, tokenInfo.address); //Get ERC20 Token contract instance
            
            //https://web3js.readthedocs.io/en/v1.3.0/web3-eth-contract.html#id29
            MyContract.methods.transfer(toAddress, total) 
                .send({from: botSettings.botAddress})
                .on('transactionHash', function(hash){
                    console.log( `Faucet reclamado por ${amount} ${botPrefix.toUpperCase()} TX:` , hash);
                    msg = _lang(`:gift: <@!${user_id}> you have received **${amount} ${botPrefix.toUpperCase()}**, try again in **6 hours**`, `:gift: <@!${user_id}> has recibido **${amount} ${botPrefix.toUpperCase()}**, intenta de nuevo en **6 horas**`, serverId);
                    message.channel.send(msg);
                })
                // .on('confirmation', function(confirmationNumber, receipt){
                //     console.log("Confirmation Num:",confirmationNumber);
                //     console.log("Receipt:",receipt.transactionHash);
                // })
                // .on('receipt', function(receipt){
                //     console.log("Receipt:",receipt.transactionHash);
                // })
                .on('error', function(error, receipt) { // If the transaction was rejected by the network with a receipt, the second parameter will be the receipt.
                    console.log("Error enviando tip:",error);
                    console.log("Receipt:",receipt);
                });
                
        } else {
            return message.channel.send(_lang(":thinking: sorry, you are not yet registered in the bot.", ":thinking: lo siento, aún no estas registrado.", serverId));
        }
        
    }



//-----------------------
// HELP Commands
//-----------------------

//-- Display Token info and contracts
    if(message.content.toLowerCase() === botPrefix+"getlist" || 
        message.content.toLowerCase() === botPrefix+"tokenlist"){

        //console.log(botSettings.tokens);
        let obj = botSettings.tokens;
        let tokenData = "";

        Object.keys(obj).forEach(key=>{
            //console.log(`${key} : ${obj[key]["address"]}`);
            if(key!="WEXP" && key!="EXC"){
                tokenData = `${tokenData}**${key}** (${obj[key]["name"]})\nContract address: \`\`\`${obj[key]["address"]}\`\`\`\n`; 
                //tokenData = `${tokenData}${key} (${obj[key]["name"]})\nContract address: ${obj[key]["address"]}\n\n`; 
            }
        });

        message.channel.send(`${tokenData}`);
    }

//-- Display Token info and contracts
    if(message.content.toLowerCase() === botPrefix+"info" || 
        message.content.toLowerCase() === botPrefix+"token"){

        const tokenInfo = botSettings.tokens[tokenKeyName];
        //console.log(tokenKeyName,tokenInfo);
        message.channel.send(
            `**${tokenKeyName}** (${tokenInfo.name})\n**Decimals:** ${tokenInfo.decimals}\n**Total supply:** ${tokenInfo.supply}\n**Contract address:**`
        );

        // if(botPrefix=="exc."){
        //     message.channel.send(
        //         `\`\`\`css\n ******** Coming Soon ********** \`\`\``
        //     );
        // }else{
            message.channel.send(
                `\`\`\`css\n${tokenInfo.address}\`\`\``
            );
        //}
        
        
        /*
        //console.log(botSettings.tokens);
        let obj = botSettings.tokens;
        let tokenData = "";
        Object.keys(obj).forEach(key=>{
            //console.log(`${key} : ${obj[key]["address"]}`);
            if(key!="WEXP"){
                tokenData = `${tokenData} **${key}** (${obj[key]["name"]}) \n **Contract address:** ${obj[key]["address"]} \n **Decimals:** ${obj[key]["decimals"]} \n **Total supply:** ${obj[key]["supply"]} \n\n`; 
            }
        });
        message.channel.send(tokenData);
        */
    }

//-- Display commands list
    if(message.content.toLowerCase() === botPrefix+"help" || 
       message.content.toLowerCase() === botPrefix+"ayuda"){
        message.channel.send(
            
            _lang(
                ":robot: Bot Commands:\n\n"+
                "**"+botPrefix+"register** *<address>* -  regirter your Expanse Address, same address to store your Expanse Tokens. \n"+
                "**"+botPrefix+"bal** *<address>* -  show your token balances. \n"+ 
                "**"+botPrefix+"changeadd** *<address>* -  update your Expanse Address.\n"+ 
                "**"+botPrefix+"info** -  show token info.\n"+
                "**"+botPrefix+"rain** *<amount>* -  send tokens to all registered users (only Admins).\n"+ 
                "**"+botPrefix+"tip** *<user>* *<amount>* -  send tokens to anothe user (only Admins)\n\n"+
                "**"+botPrefix+"getlist** -  display token list available in the bot.\n"+
                "**"+botPrefix+"getinfo** -  show bot info, address and link to balances by token.\n"+
                "**"+botPrefix+"getaddr** -  show Expanse bot Address. \n" +
                "**"+botPrefix+"getbal** -  show current Bot token balance. \n" + 
                "**"+botPrefix+"getid** -  show User ID.\n\n"+
                "```**NOTICE: the bot does not store the coins or tokens, all tokens will send directly to your registered wallet, please keep safe your private key. Losing your private key is same to lose your tokens.**```" ,

                ":robot: Comandos para usar en PRM Bot:\n\n"+
                "**"+botPrefix+"register** *<address>* -  registra tu dirección EXP, en esa misma quedan tambien los tokens. \n"+
                "**"+botPrefix+"bal** *<address>* -  muestra el balance de tus tokens. \n"+ 
                "**"+botPrefix+"changeadd** *<address>* -  actualiza tu dirección de EXP.\n"+ 
                "**"+botPrefix+"info** -  muestra información del token.\n"+
                "**"+botPrefix+"rain** *<amount>* -  envía tokens a todos los usuarios registrados (solo Admins).\n"+ 
                "**"+botPrefix+"tip** *<user>* *<amount>* -  envía tokens al usuario indicado (solo Admins)\n\n"+
                "**"+botPrefix+"getlist** -  muestra lista de tokes disponibles en el bot.\n"+
                "**"+botPrefix+"getinfo** -  muestra información del bot, dirección y balances de cada token.\n"+
                "**"+botPrefix+"getaddr** -  muestra la dirección de Expanse del bot que contiene los tokens. \n" +
                "**"+botPrefix+"getbal** -  muestra el balance del token en el wallet del bot. \n" + 
                "**"+botPrefix+"getid** -  muestra el ID del usuario.\n\n"+
                "```**NOTA: El bot de Promineros no guarda tus monedas o tokens, todo se envia directamente a tu wallet. Por favor guarda bien tu llave privada del wallet, si pierdes las llaves puedes perder tus tokens.**```" ,

                serverId
            )

        );
    }


//-----------------------
// COMANDOS PARA EL BOT
//-----------------------

//-- Display bot info
    if(message.content.toLowerCase() === botPrefix+"getinfo"){
        message.channel.send(
            _lang(`:robot: Bot info: \n **Prefijo:** ${botPrefix.toUpperCase()} \n **Bot address: ** ${botSettings.botAddress} \n **Balances** https://expexplorer.guarda.co/address/${botSettings.botAddress}`,
                `:robot: Información del bot actual: \n **Prefijo:** ${botPrefix.toUpperCase()} \n **Bot address: ** ${botSettings.botAddress} \n **Balances** https://expexplorer.guarda.co/address/${botSettings.botAddress}`,
                serverId)
        );
    }

//-- Display bot wallet
    if(message.content.toLowerCase() === botPrefix+"getaddr" ||
       message.content.toLowerCase() === botPrefix+"getaddress"){
        message.channel.send(
            _lang(`:robot: The bot address is: **${botSettings.botAddress}** \n Please donate some Tokens or Expanse for this amazing community, thank you!`,
                  `:robot: La dirección es: **${botSettings.botAddress}** \n Si deseas puedes donar tokens o Expanse para la comunidad, gracias!`,
                  serverId)
        );
    }

//-- Get discord user id.
    if(message.content.toLowerCase().startsWith(botPrefix+"getid")){
        //console.log(args[1]); <@!300412335275769856>
        if(args[1]){
            let user_id = args[1].replace(/\D/g, ""); //extraer el id del usuario
            return message.channel.send(`${args[1]} tu ID discord es: ${user_id}`);    
        }else{
            return message.channel.send(`<@!${userID}> tu ID discord es: ${userID}`);    
        }
    }

//-- Get BOT token balances
    if(message.content.toLowerCase() === botPrefix+"getbal" || 
        message.content.toLowerCase() === botPrefix+"getbalance"){

        //Get current token info
        const tokenInfo = botSettings.tokens[tokenKeyName];
        //console.log("Token Info:", tokenInfo);
        //console.log("Obj: ",tokenInfo);

        //const data = getJson(botSettings.usersById);
        const walletAddress = botSettings.botAddress;

        if(tokenInfo === undefined){
            return message.channel.send("Lo sentimos, el token o la moneda aun no está dispobinle en nuestro bot.");
        }

        if(walletAddress === undefined){
            return message.channel.send("No hay wallet registrada para este bot.");
        }

        //Get Token balance            
        //Get ERC20 Token contract instance
        //const contract = new web3.eth.Contract(minABI, tokenAddress, {from:walletAddress});
        const MyContract = new web3.eth.Contract(minABI, tokenInfo.address);

        // Call balanceOf function
        MyContract.methods.balanceOf(walletAddress).call()
            .then(function(balance){
                //console.log( "Total PRM:", (balance/Math.pow(10,18)).toFixed(3) );                
                balance = (balance/Math.pow(10,tokenInfo.decimals)).toFixed(8);

                message.channel.send( 
                    _lang( `:robot: Bot balance is **${balance} ${botPrefix.toUpperCase()}**, would be nice if you donate me some tokens to share with this amazing community, type command **${botPrefix}getaddress** to display my wallet.`, 
                           `:robot: El bot tiene **${balance} ${botPrefix.toUpperCase()}**, seria genial si me donas algunos tokens para compartir con la comunidad, escribe el comando **${botPrefix}getaddress** para ver mi wallet.`, 
                           serverId ) 
                );
            });
    }



});





//-- FUNCTIONS --

function getJson(path){
	return JSON.parse(fs.readFileSync(path));
}


function sendToken(tokenAddress, toAddress, amount){    
    // Use BigNumber
    let weiAmount = amount*Math.pow(10,18);
    let total = web3.utils.toBN(weiAmount).toString(); //https://web3js.readthedocs.io/en/v1.3.0/web3-utils.html?highlight=toBN#tobn

    //console.log("Amount: ", weiAmount);
    //console.log("Total: ", weiAmount);

    //Get ERC20 Token contract instance
    //const contract = new web3.eth.Contract(minABI, tokenAddress, {from:walletAddress});
    const MyContract = new web3.eth.Contract(minABI2, tokenAddress);
  
    //gas: web3.utils.toHex(120000), //120000
    MyContract.methods.transfer(toAddress, total)
        .send({from: '0xD4dbE54c51445E13ea0aa43c13A3b23BA5613baa'})
        .then(function(receipt){
            // receipt can also be a new contract instance, when coming from a "contract.deploy({...}).send()"
            console.log("RESULTADO: ", receipt);
        });
}

// Main sending function.
function sendCoins(address,value,message,userId){
/*
    console.log("enviado:"+ value);	
	console.log("fixed:"+ value.toFixed(8) );	
	console.log("toString:"+ numberToString(value) );	

	web3.eth.sendTransaction({
	    from: botSettings.address,
	    to: address,
	    gas: web3.utils.toHex(120000), //120000
	    value: numberToString(value) //(value).toFixed(8) //
	})
	.on('transactionHash', function(hash){
		// sent pm with their tx
		// recive latest array
		if(userId != 1){
			let fValue = value/Math.pow(10,18).toFixed(8);
			//let username = bot.users.find('username',name);
			let author = bot.users.find('id',userId);
			//author.send("Hi "+name+" , you are lucky man.\n Check hash: https://explorer.expanse.tech/tx/"+ hash);
			
			//author.send("Hola **"+getUsernameById(userId)+"**, eres afortunado, has recibido una lluvia de **"+fValue+" EXP**.\n :moneybag: Revisa la transacción en: https://explorer.expanse.tech/tx/"+ hash + " \n \n **Conoce la últimas noticas y actualizaciones en EXPANSE** https://expanse.tech/expanse-newsletter-vol-4-no-17-09-30-2019/");
			author.send("Hola **"+getUsernameById(userId)+"**, eres afortunado, has recibido una lluvia de **"+fValue+" EXP**.\n :moneybag: Revisa la transacción en: https://explorer.expanse.tech/tx/"+ hash);
			author.send("Ya probaste el nuevo faucet de Expanse para Android? completa 5000 puntos XP y reclama EXP directamente en tu walelt. https://play.google.com/store/apps/details?id=com.expfaucet");
			//author.send("Invita a tus amigos o familiares para que el equipo de ProMineros realice mas lluvias y sorteos. https://cdn.discordapp.com/attachments/527515942251659266/629676145813618689/lluvia-de-invitados.jpg");
			//author.send("https://media.discordapp.net/attachments/527515942251659266/632584848275537920/dia-lluvioso.jpg");
		} else {
			message.channel.send(":moneybag: tip enviado! revisa tu transacción en: https://explorer.expanse.tech/tx/"+ hash); //"Tip was sent. \n Check hash: https://explorer.expanse.tech/tx/"+ hash
		}
	})
    .on('error', console.error);
    */
}


// Raining command to send users coin.
function raining(amount, message, tokenKeyName, userName, serverId){
    const onlineUsers = getOnline(); // online users
	const data = getJson(botSettings.usersById); // registered users

	// Create online and register array user ids
	//var onlineAndRegister = Object.keys(data).filter(username => {return onlineUsers.indexOf(username)!=-1});
	let onlineAndRegister = Object.keys(data).filter(id => {return onlineUsers.indexOf(id)!=-1});    
    
    // create object with name - address and name - values
	let latest = {};
	for (let theUser of onlineAndRegister) {
	  if (data[theUser]) {
	    latest[data[theUser]] = theUser;
	  }
	}

	//if(Object.keys(onlineUsers).length > 0){
        //Get current token info
        let tokenInfo = botSettings.tokens[tokenKeyName];
        console.log("Token Info:", tokenInfo);

		// if use wrong amount (string or something)
		let camount = amount/Object.keys(latest).length;
        let weiAmount = 0;

        if(tokenKeyName.toUpperCase()=="EABN"){
            camount = camount.toFixed(6);
            weiAmount = camount*Math.pow(10,tokenInfo.decimals).toFixed(6);
        }else{
            camount = camount.toFixed(8);
            weiAmount = camount*Math.pow(10,tokenInfo.decimals).toFixed(8);
        }

        let theGasLimit = 260000;
        if(tokenKeyName.toUpperCase()=="EVLTZ"){
            theGasLimit = 3000000;
        }
        if(tokenKeyName.toUpperCase()=="EXC"){
            theGasLimit = 5000000;
        }

        let total = web3.utils.toBN(weiAmount).toString(); //https://web3js.readthedocs.io/en/v1.3.0/web3-utils.html?highlight=toBN#tobn

        const MyContract = new web3.eth.Contract(minABI2, tokenInfo.address); //Get ERC20 Token contract instance

		//message.channel.send( `:thunder_cloud_rain: Ha caido una **lluvia** de ${tokenKeyName}'s sobre **${Object.keys(latest).length}** usuarios online!!. confirma si lo has recibido en tu DM (Mensaje Directo).` ); 
        console.log(`Rain hecho por: ${userName} de ${amount} ${tokenKeyName}` );
        
        //Por cada usuario enviar el rain
        let sentUsers = "";
        let sentUsers2 = "";
        let i = 0;

        for(const toAddress of Object.keys(latest)){
            let user_id = latest[toAddress];
            
            //sendCoins(address, weiAmount, message, user_id);
            if(i<=80){
                sentUsers += `<@!${user_id}> `;
            }else{
                sentUsers2 += `<@!${user_id}> `;
            }
            i++;

            //https://web3js.readthedocs.io/en/v1.3.0/web3-eth-contract.html#id29
            MyContract.methods.transfer(toAddress, total) 
                .send({from: botSettings.botAddress, gas: theGasLimit})
                .on('transactionHash', function(hash){
                    console.log( `Rain enviado de ${tokenKeyName} TX:`, hash);
                    //msg = `:tada: <@!${user_id}> has recibido un tip de **${amount} ${botPrefix.toUpperCase()}**`;
                    //message.channel.send(msg);
                })
                //.on('confirmation', function(confirmationNumber, receipt){
                //    console.log("Confirmation Num:",confirmationNumber);
                //    console.log("Receipt:",receipt.transactionHash);
                //})
                //.on('receipt', function(receipt){
                //    console.log("Receipt:",receipt.transactionHash);
                //})
                .on('error', function(error, receipt) { // If the transaction was rejected by the network with a receipt, the second parameter will be the receipt.
                    console.log("Error enviando rain:",error);
                    console.log("Receipt:",receipt);
                });
        }

        console.log("Rain enviado:", sentUsers);
        console.log("Rain each:", weiAmount);
        message.channel.send(
            _lang(`:thunder_cloud_rain: It's raining **${amount} ${tokenKeyName}** on users: ${sentUsers}, each one has received **${camount} ${tokenKeyName}**`,
                  `:thunder_cloud_rain: Está lloviendo **${amount} ${tokenKeyName}** sobre los usuarios: ${sentUsers}, cada uno ha recibido **${camount} ${tokenKeyName}**`,
                  serverId) );
        if(sentUsers2){
            message.channel.send(
                _lang(`:thunder_cloud_rain: ${sentUsers2}, each one has received **${camount} ${tokenKeyName}**`,
                      `:thunder_cloud_rain: ${sentUsers2}, cada uno ha recibido **${camount} ${tokenKeyName}**`,
                      serverId) );
        }

	//}else{
	//	message.channel.send(":thinking: ups! no hay usuarios online.");
	//}
}


// RainingOnline command to send users coin.
function rainingOnline(amount, message, tokenKeyName, userName){
    const onlineUsers = getOnline(); // online users
	const data = getJson(botSettings.usersById); // registered users

	// Create online and register array user ids
	//var onlineAndRegister = Object.keys(data).filter(username => {return onlineUsers.indexOf(username)!=-1});
	let onlineAndRegister = Object.keys(data).filter(id => {return onlineUsers.indexOf(id)!=-1});    
    
    // create object with name - address and name - values
	let latest = {};
	for (let theUser of onlineAndRegister) {
	  if (data[theUser]) {
	    latest[data[theUser]] = theUser;
	  }
	}
/*
	//if(Object.keys(onlineUsers).length > 0){
        //Get current token info
        let tokenInfo = botSettings.tokens[tokenKeyName];
        console.log("Token Info:", tokenInfo);

		// if use wrong amount (string or something)
		let camount = amount/Object.keys(latest).length;
        camount = camount.toFixed(8);
        let weiAmount = camount*Math.pow(10,tokenInfo.decimals).toFixed(8);

        let total = web3.utils.toBN(weiAmount).toString(); //https://web3js.readthedocs.io/en/v1.3.0/web3-utils.html?highlight=toBN#tobn

        const MyContract = new web3.eth.Contract(minABI2, tokenInfo.address); //Get ERC20 Token contract instance

		//message.channel.send( `:thunder_cloud_rain: Ha caido una **lluvia** de ${tokenKeyName}'s sobre **${Object.keys(latest).length}** usuarios online!!. confirma si lo has recibido en tu DM (Mensaje Directo).` ); 
        console.log(`Rain hecho por: ${userName} de ${amount} ${tokenKeyName}` );
        
        //Por cada usuario enviar el rain
        let sentUsers = "";
        let sentUsers2 = "";
        let i = 0;

        for(const toAddress of Object.keys(latest)){
            let user_id = latest[toAddress];
            
            //sendCoins(address, weiAmount, message, user_id);
            if(i<=80){
                sentUsers += `<@!${user_id}> `;
            }else{
                sentUsers2 += `<@!${user_id}> `;
            }
            i++;

            //https://web3js.readthedocs.io/en/v1.3.0/web3-eth-contract.html#id29
            MyContract.methods.transfer(toAddress, total) 
                .send({from: botSettings.botAddress})
                .on('transactionHash', function(hash){
                    console.log( `Rain enviado de ${tokenKeyName} TX:`, hash);
                    //msg = `:tada: <@!${user_id}> has recibido un tip de **${amount} ${botPrefix.toUpperCase()}**`;
                    //message.channel.send(msg);
                })
                //.on('confirmation', function(confirmationNumber, receipt){
                //    console.log("Confirmation Num:",confirmationNumber);
                //    console.log("Receipt:",receipt.transactionHash);
                //})
                //.on('receipt', function(receipt){
                //    console.log("Receipt:",receipt.transactionHash);
                //})
                .on('error', function(error, receipt) { // If the transaction was rejected by the network with a receipt, the second parameter will be the receipt.
                    console.log("Error enviando rain:",error);
                    console.log("Receipt:",receipt);
                });
        }

        console.log("Rain enviado:", sentUsers);
        console.log("Rain each:", weiAmount);
        message.channel.send(`:thunder_cloud_rain: Promineros ha lanzado un rain de **${amount} ${tokenKeyName}**, para los usuarios: ${sentUsers}, cada uno ha recibido **${camount} ${tokenKeyName}**`);
        if(sentUsers2){
            message.channel.send(`:thunder_cloud_rain: Promineros ha lanzado un rain de **${amount} ${tokenKeyName}**, para los usuarios: ${sentUsers2}, cada uno ha recibido **${camount} ${tokenKeyName}**`);
        }

	//}else{
	//	message.channel.send(":thinking: ups! no hay usuarios online.");
	//}
*/
}

// return array with names of online users
function getOnline(){
    const data = getJson(botSettings.usersById); // registered users
    let result = [];

    //data.forEach( aUsers =>{
    //    console.log(aUsers);
    //});

    for (let key in data){
        if(data.hasOwnProperty(key)){
          //console.log(`${key} : ${data[key]}`)
          result.push(key);
        }
    }

    //console.log(result);
    return result;

    /*
    //https://stackoverflow.com/questions/64349914/get-guild-members-and-filter-them-discord-js
    let guild = bot.guilds.cache.get('537789773994131467'); //527515942251659264 
    let userCount = guild.memberCount;
    //let onlineCount = guild.members.cache.filter(member => member.presence.status === 'online').size;
    let foo = [];

    //https://discord.js.org/#/docs/main/12.5.1/class/GuildMemberManager
    //guild.members.fetch({force:true})
    //    .then(console.log)
    //    .catch(console.error);
                
    // Fetch by an array of users including their presences
    // TO DO: falta que presence.status retorne los usuario online //, 
    guild.members.fetch({ user: ['300412335275769856', '419467013728108544', '408912102443450368'], withPresences: true, cache: false, force: true })
        .then(rUsers => {
            //console.log("MEMBER:",m)
            rUsers.forEach((rUser) => {
                console.log("M ID", rUser.id);
                console.log("M PRESENSE", rUser.presence.status);
            })
        })
        .catch(console.error);
    */
    /*
    //console.log("MEMBER CACHE",guild.members.cache);
    guild.members.cache.forEach((member) => {
        //console.log("VAL ID",member.id);
        //console.log("VAL PRESENSE",member.presence.status);
        console.log("M STATUS:",member.presence.status);
		if(member.presence.status === "online"){
			foo.push(member.id);
		}
    });
    console.log(foo);
    */

    //console.log("userCount:",userCount);
    //console.log("onlineCount",onlineCount);
    //return;
}

/*
function getOnline(){
	let foo = [];
    //let users = bot.users;
    let guilds = bot;

    console.log("El bot: ",guilds);
    return;

	users.keyArray().forEach((val) => {
		let userName = users.get(val).id;
		let status = users.get(val).presence.status;
		if(status == "online"){
			foo.push(userName);
		}
    });
    
	return foo;
}
*/

function _lang(en, es, serverId){
    //527515942251659264  PRM Server ID
    //351390305561346060 EXP Server ID
    let result = en;

    if( serverId=="527515942251659264" || //Promineros Server 
        serverId=="866061366137454623" //Sugar Server
    ){
        result = es;
    }
    
    return result;
}

function isChannelAccess(channelID){
    let result = false;
    if (channelID === '834178684578824233' || // EXC
        channelID === '694988313735725138' || // SVIT
        channelID === '854397978549551114' || // XTC in Expanse
        channelID === '875078459179814962' || // SUGAR in Expanse        
        channelID === '865749142977511474' || // RARE in Expanse
        channelID === '866126354402377730' || // SUGAR in Sugar general channel
        channelID === '828073825371029514' || // ALYM Airdrop
        channelID === '813450473260580904' || // EXPANSE Tokens #PRM
        channelID === '424253832160083996' || // EXPANSE #bot-commands
        channelID === '375595824370941953' || // EXPANSE #spanish-language
        channelID === '780959976695922719' || // EXPANSE #steak
        channelID === '825362595288055838' || // EXPANSE #alym
        channelID === '614616294897221652' || // PRM test
        channelID === '527521242769653760' || // PRM -bots
        channelID === '527515942251659266' || // PRM -general
        channelID === '527545622006464512' || // PRM -sorteos
        channelID === '527540917083832320' || // PRM -rains
        channelID === '789589426794659910' || // PRM hy511
        channelID === '797512250893664256' || // PRM svit
        channelID === '537789774593785858' || // PRM-Testing general
        channelID === '538132696489000980' || // PRM-Testing bot-test
        channelID === '835610160788406333' || // PRM-faucet
        channelID === '925049057309777960'  // SVIT-faucet
    ){
        result = true;
    }
    return result;
}

function isTokenOwner( userID ){
    let result = false;
    if( userID=="370711244245565441" || //Anabel
        userID=="339843008323256322" || //Ruul 
        userID=="300412335275769856" || //Bitjohn
        userID=="351489283166699520" || //Omar
        userID=="419467013728108544" || //Mello
        userID=="547477878804054028" || //JJJH82
        userID=="360276639147360266" || //madwillysg
        userID=="537600835966861314" || //Wedergarten ALYM
        userID=="778048794625179690" || //Stefal EABN
        userID=="627983177054420993" || //Jordan16452 RARE
        userID=="410761149349363731" || //Tego SUGAR
        userID=="734675415276322837" || //Blockchain Honduras (XTC)
        userID=="804032424215969822" //Just a Seal x420
    ){
        result = true;
    }
    return result;
}


bot.login(botSettings.discordToken);